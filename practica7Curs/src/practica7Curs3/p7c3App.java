package practica7Curs3;

import java.util.Enumeration;
import java.util.Hashtable;

import javax.swing.JOptionPane;

public class p7c3App {

	public static void main(String[] args) {
		//Aqu� creo las variables que posteriormente pondremos en uso
		String eleccion;
		String nombreProducto;
		//Aqu� creo la baseDeDatos de todos los productos usando una Hashtable. La lista contendr� 10 productos como base
		Hashtable<String, Double> baseDeDatos;
		baseDeDatos = new Hashtable<String, Double>(){
		    {
		        put("Sprite", 2.5);
		        put("CocaCola", 1.7);
		        put("Cereales", 3.0);
		        put("Papadeltas", 1.5);
		        put("Limones", 2.7);
		        put("Aceitunas", 2.8);
		        put("Tomates", 2.6);
		        put("Patatas", 5.5);
		        put("Naranjas", 1.2);
		        put("Berenjenas", 2.9);
		    }
		};
		//Aqu� creo un bucle tipo do/while que asegurar� la salida del programa cuando el usuario lo indique
		do {
			//Aqu� pregunto al Usuario que opci�n de las 4 disponibles va a usar a trav�s de n�meros y un switch
			eleccion = JOptionPane.showInputDialog(null, "1: A�adir producto | 2: Mostrar producto concreto | 3: Mostrar productos | 4: Salir");
			int eleccionInt = Integer.parseInt(eleccion);
			

			
			//Aqu� creo un switch donde se ejecutar�n las funciones de cada "case" dependiendo de que opci�n elija el usuario
			switch (eleccionInt) {
			case 1:
				nombreProducto = JOptionPane.showInputDialog(null, "Que nombre de producto quieres a�adir?");
				String precio = JOptionPane.showInputDialog(null, "Que precio tiene ?");
				double precioDouble = Double.parseDouble(precio);
				baseDeDatos.put(nombreProducto,precioDouble);
				break;
			case 2:
				String mostrarProductoBDD = JOptionPane.showInputDialog(null, "Que nombre de producto quieres ver su precio?");
				mostrarProducto(mostrarProductoBDD,baseDeDatos);
				break;
			case 3:
				mostrarLista(baseDeDatos);
				break;

			default:
				break;
			}
		}
		while(!eleccion.contentEquals("4"));
	

	}
	
	//Aqu� creo una funci�n donde muestro Hashtable del producto por consola utilizando un System.out junto con la herramienta .get
	public static void mostrarProducto(String mostrarProductoUsuario, Hashtable<String, Double> baseDeDatosUsuario) {
		
		
	System.out.println(baseDeDatosUsuario.get(mostrarProductoUsuario));
		
	}
	//Aqu� muestro la hashtable de toda la lista final donde se muestran las ids y los valores
	public static void mostrarLista(Hashtable<String, Double> baseDeDatosUsuario) {
		

		Enumeration<Double> productos = baseDeDatosUsuario.elements();
        Enumeration<String> ids = baseDeDatosUsuario.keys();

        while (ids.hasMoreElements()) {
            System.out.println("El articulo: " + ids.nextElement() + ", tiene una precio de: " + productos.nextElement());

        }
		
	}

}
