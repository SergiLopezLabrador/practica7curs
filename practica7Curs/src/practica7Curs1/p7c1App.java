package practica7Curs1;

import java.util.Enumeration;
import java.util.Hashtable;

import javax.swing.JOptionPane;

public class p7c1App {

	public static void main(String[] args) {
		//Aqu� creo las variables que utilizaremos para este programa, entre ellas, algunas pedidas al propio usuario
		String alumnosCantidad = JOptionPane.showInputDialog(null, "Cuantos alumnos quieres para saber sus notas: ");
		int alumnosCantidadInt = Integer.parseInt(alumnosCantidad);
		//Aqu� creo la hashtable, donde posteriormente la mostraremos por consola
		Hashtable<String,String> tablaDeNotas = new Hashtable<String, String>();
		//Aqu� hago un bucle tipo for donde pregunto el nombre del alumno, sus notas y compruebo su media a trav�s de la funci�n
		for (int i = 0; i < alumnosCantidadInt; i++) {
			String idAlumno = JOptionPane.showInputDialog(null, "Introduce la ID del alumno: ");
			
			int notaAlumnoMedia = mediaNotas();
			String notaFormatoString = String.valueOf(notaAlumnoMedia);
			//Aqu� a�adimos los datos a la hashtable
			tablaDeNotas.put(idAlumno, notaFormatoString);
		}

		mostrarListaFinal(tablaDeNotas);

	}
	//En esta funci�n calculo la media de las notas de cada alumno que pide el Usuario
	public static int mediaNotas() {
		int notaMedia = 0;
		for (int i = 0; i < 4; i++) {
			String notaAlumno = JOptionPane.showInputDialog(null, "Introduce la nota del alumno: ");
			int notaAlumnoInt = Integer.parseInt(notaAlumno);
			notaMedia = notaMedia + notaAlumnoInt;
		}
		notaMedia=(notaMedia)/4;
		return notaMedia;
	}
	//En esta funci�n mostraremos la lista con las notas medias a trav�s de la consola
	public static void mostrarListaFinal(Hashtable<String,String> tablaDeNotasUsuario) {
		Enumeration<String> idPrimaria = tablaDeNotasUsuario.keys();
		Enumeration<String> enumeration = tablaDeNotasUsuario.elements();
		while(enumeration.hasMoreElements()) {
			System.out.println(" " + "ID: " + idPrimaria.nextElement() + " con una media de " + enumeration.nextElement());
		}
	}
	
	
	


}
