package practica7Curs4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.swing.JOptionPane;

public class p7c4App {

	public static void main(String[] args) {
		//Aqu� creo las variables que posteriormente pondremos en uso
		String eleccion;
		String nombreProducto;
		Hashtable<String, Double> baseDeDatos;
		//Aqu� creo la baseDeDatos de todos los productos usando una Hashtable. La lista contendr� 10 productos como base
		baseDeDatos = new Hashtable<String, Double>(){
		    {
		        put("Sprite", 2.5);
		        put("CocaCola", 1.7);
		        put("Cereales", 3.0);
		        put("Papadeltas", 1.5);
		        put("Limones", 2.7);
		        put("Aceitunas", 2.8);
		        put("Tomates", 2.6);
		        put("Patatas", 5.5);
		        put("Naranjas", 1.2);
		        put("Berenjenas", 2.9);
		    }
		};
		//Aqu� creo un bucle tipo do/while que asegurar� la salida del programa cuando el usuario lo indique
		do {
			//Aqu� pregunto al Usuario que opci�n de las 5 disponibles va a usar a trav�s de n�meros y un switch
			eleccion = JOptionPane.showInputDialog(null, "1: A�adir producto | 2: Mostrar producto concreto | 3: Mostrar productos | 4: Salir | 5: Comprar producto");
			int eleccionInt = Integer.parseInt(eleccion);
			

			
			//Aqu� creo un switch donde se ejecutar�n las funciones de cada "case" dependiendo de que opci�n elija el usuario
			switch (eleccionInt) {
			case 1:
				nombreProducto = JOptionPane.showInputDialog(null, "Que nombre de producto quieres a�adir?");
				String precio = JOptionPane.showInputDialog(null, "Que precio tiene ?");
				double precioDouble = Double.parseDouble(precio);
				baseDeDatos.put(nombreProducto,precioDouble);
				break;
			case 2:
				String mostrarProductoBDD = JOptionPane.showInputDialog(null, "Que nombre de producto quieres ver su precio?");
				System.out.println(baseDeDatos.get(mostrarProductoBDD));
				break;
			case 3:
				Enumeration<Double> productos = baseDeDatos.elements();
		        Enumeration<String> ids = baseDeDatos.keys();

		        while (ids.hasMoreElements()) {
		            System.out.println("El articulo: " + ids.nextElement() + ", tiene una precio de: " + productos.nextElement());

		        }
				break;
			case 5:
				ex2(baseDeDatos);
				break;

			default:
				break;
			}
		}
		while(!eleccion.contentEquals("4"));
	

	}
	

	//Aqu� creo un m�todo donde su funci�n ser� principalmente preguntar al Usuario cuantos productos comprar� y mostrar la lista final
	//por consola usando un bucle tipo for junto a la herramienta System.out. Al final de la lista de productos comprados
	//mostrar� lo que ha costado y lo que recibes de cambio
	public static void ex2(Hashtable<String, Double> baseDeDatosUsuario) {
		

		String productosComprar = JOptionPane.showInputDialog(null, "Cuantos productos quieres comprar?: ");
		int productosComprarInt = Integer.parseInt(productosComprar);
		double pagarFinal = 0;
		
		ArrayList<String[]> productoFinal = new ArrayList<String[]>();
		System.out.println("PRODUCTOS COMPRADOS");
		for (int i = 0; i < productosComprarInt; i++) {
			productoFinal.add(anadirProductos(baseDeDatosUsuario));
			pagarFinal = pagarFinal + Double.parseDouble(productoFinal.get(i)[3]);
		}
		
		
		String hasPagado = JOptionPane.showInputDialog(null, "Debes pagar: " + pagarFinal);
		double hasPagadoDouble = Double.parseDouble(hasPagado);
		mostrarArray(productoFinal, productosComprarInt);
		System.out.println("Pagaste " + hasPagadoDouble + " tu cambio �s: " + (hasPagadoDouble - pagarFinal));
		
		
	}
	//Aqu� crear� un m�todo donde preguntar� al usuario tanto el precio del producto, como el IVA y el nombre del mismo.
	//Tambi�n, en el caso que quiera comprar, comprobar� si el producto que quiere comprar existe en la lista
	//en el caso de que no existiese, no te lo dejar� comprar
	public static String[] anadirProductos(Hashtable<String, Double> baseDeDatosUsuario) {
		String[] listaProductosTienda = new String[4];
		String ivaProducto;
		String nomProducte;

		do {
			nomProducte = JOptionPane.showInputDialog(null, "Que nombre tiene el producto?: ");

		}
		while(baseDeDatosUsuario.get(nomProducte) == null);
		
		
		listaProductosTienda[0] = nomProducte;
			ivaProducto = JOptionPane.showInputDialog(null, "Que IVA quieres tiene, 21 o 4: ");
		listaProductosTienda[1] = ivaProducto;
		String valorProducto = String.valueOf(baseDeDatosUsuario.get(nomProducte));
		listaProductosTienda[2] = valorProducto;
		Double valorConIva = (((Double.parseDouble(valorProducto)/100)* Double.parseDouble(ivaProducto)) + Double.parseDouble(valorProducto));
		listaProductosTienda[3] = String.valueOf(valorConIva);
	
		return listaProductosTienda;
	
}
	//Aqu� he creado un m�todo donde su funci�n es mostrar la lista final de todos los productos con toda su informaci�n
	//por consola de forma estructurada
	public static void mostrarArray(ArrayList<String[]> productoFinalUsuario, int productosComprarIntUsuario) {
		
		
        for (int i = 0; i < productosComprarIntUsuario; i++) {
            for (int j = 0; j < productosComprarIntUsuario; j++) {
                System.out.println("Producto comprado: " + productoFinalUsuario.get(i)[0]);
                System.out.println("IVA del producto: " + productoFinalUsuario.get(i)[1]);
                System.out.println("Precio del producto: " + productoFinalUsuario.get(i)[2]);
                System.out.println("Precio con IVA del producto: " + productoFinalUsuario.get(i)[3]);
            }
            System.out.println(" ");
        }
	}

}
