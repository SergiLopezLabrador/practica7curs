package practica7Curs2;

import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JOptionPane;

public class p7c2App {

	public static void main(String[] args) {
		//Aqu� creo las variables que posteriormente pondremos en uso
		String productosComprar = JOptionPane.showInputDialog(null, "Cuantos productos quieres comprar?: ");
		int alumnosCantidadInt = Integer.parseInt(productosComprar);
		double pagarFinal = 0;
		//Aqu� creamos una ArrayList de strings donde dentro habr� una array por cada producto que ha insertado el usuario
		ArrayList<String[]> productoFinal = new ArrayList<String[]>();
		System.out.println("PRODUCTOS COMPRADOS");
		for (int i = 0; i < alumnosCantidadInt; i++) {
			productoFinal.add(anadirProductos());
			pagarFinal = pagarFinal + Double.parseDouble(productoFinal.get(i)[3]);
		}
		
		//Aqu� mostramos los mensajes finales sobre el dinero que tiene que pagar y el cambio que le pertenece
		String hasPagado = JOptionPane.showInputDialog(null, "Debes pagar: " + pagarFinal);
		double hasPagadoDouble = Double.parseDouble(hasPagado);
		mostrarArray(productoFinal);
		System.out.println("Pagaste " + hasPagadoDouble + " tu cambio �s: " + (hasPagadoDouble - pagarFinal));
		
		

	}
	//Aqu� crear� una funci�n donde preguntar� al usuario tanto el precio del producto, como el IVA y el nombre del mismo
	public static String[] anadirProductos() {
		String[] listaProductosTienda = new String[4];
		String ivaProducto;
		String nomProducte = JOptionPane.showInputDialog(null, "Que nombre tiene el producto?: ");
		listaProductosTienda[0] = nomProducte;
			ivaProducto = JOptionPane.showInputDialog(null, "Que IVA quieres tiene, 21 o 4: ");
		listaProductosTienda[1] = ivaProducto;
		String valorProducto = JOptionPane.showInputDialog(null, "Cuanto vale el producto?: ");
		listaProductosTienda[2] = valorProducto;
		Double valorConIva = (((Double.parseDouble(valorProducto)/100)* Double.parseDouble(ivaProducto)) + Double.parseDouble(valorProducto));
		listaProductosTienda[3] = String.valueOf(valorConIva);
	
		return listaProductosTienda;
	
}
	//Aqu� creo una funci�n que permite mostrar la listaArray por consola
	public static void mostrarArray(ArrayList<String[]> arrayProductes) {
		
		System.out.println(Arrays.deepToString(arrayProductes.toArray()));
	}

}
